<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PredictorController extends Controller
{
    /** 
     * Funcion principal para predecir el pico y placa. 
     * Recibe los datos del formulario en la variable $request.
     * Retorna un String del calculo pasandolo a la vista 'result'.
     * **/

    function calcularPicoyPlaca(Request $request){
        if ($request->input('placa') == null || $request->input('fecha') == null || 
            $request->input('hora') == null) {
            return view('fail');
        }
        else {
            //Obtencion del ultimo numero de la placa
            $placa = $this->obtenerDigitoPlaca($request->input('placa'));
            //Obtencion del dia de la fecha seleccionada
            $dia = $this->obtenerDia($request->input('fecha')); 
            //Verificacion si la hora ingresada esta dentro del rango de horas prohibidas
            $hora = $this->verificarHora($request->input('hora'));
            //Comparacion si el dia seleccionado coincide con el numero de la placa y la hora ingresada
            if ($this->verificarPlaca($placa, $dia) == 1 && $hora == 1 ) {
                //Retorno del resultado a la vista
                return view('result')->with('resultado', "tiene pico y placa");
            }
            else {
                //Retorno del resultado a la vista
                return view('result')->with('resultado', "no tiene pico y placa");
            }
        }
    }

    /** 
     * Funcion para obtener el ultimo dígito de la placa.
     * Recibe el numero de la placa completo.
     * Retorna el ultimo digito de la placa ingresada. 
     * **/

    function obtenerDigitoPlaca($placa){
        //Retorno del resultado
        return substr($placa,-1);
    }

    /** 
     * Funcion para obtener el dia de la fecha.
     * Recibe el string de la fecha
     * Retorna un String con el dia de la semana correspondiente a la fecha ingresada.
     * **/

    function obtenerDia($fecha) {
        //String con los dias de la semana
        $dias = array('Domingo','Lunes','Martes','Miercoles','Jueves','Viernes','Sabado');
        //Calculo del dia que corresponde a esa fecha
        $dia = $dias[date('w', strtotime($fecha))];
        //Retorno del resultado
        return $dia;
    }

    /** 
     * Funcion para verificar si la hora ingresada esta dentro de las horas restringidas. 
     * Recibe el String con la hora ingresada.
     * Retorna un Boolean del resultado.
     * El calculo se lo hizo sabiendo que las horas restringidas van desde las 7.00 - 9.30 y 
     * de 16.00 - 19.30
     * **/

    function verificarHora($hora) {
        //Obtencion de hora ingresada
        $horas = (int) substr($hora, 0, 2);
        //Obtencion de los minutos ingresados
        $minutos = (int) substr($hora, 3, 2);
        //Comparativa si esta dentro del rango de horas restringidas
        if (($horas >= 7 && $horas <= 9) || ($horas >= 16 && $horas <= 19)) {
            //Comparativa si esta en las horas limite con los minutos respectivos
            if(($minutos > 30 && $horas == 9) || ($minutos > 30 && $horas == 19)){
                //Retorno del resultado
                return false;
            }
            //Retorno del resultado
            return true;
        }
        else {
            //Retorno del resultado
            return false;
        }
    }    

    /** 
     * Funcion para verificar si el dia escogido es el de su placa 
     * Recibe dos parametros, uno con el ultimo numero de la placa, el otro con el dia de la semana
     * Retorna un Boolean dependiendo si coinciden el numero de la placa con el dia
     * **/

    function verificarPlaca($numplaca, $dia) {
        //Analisis del dia de la semana
        switch($dia){
            case 'Lunes':
                //Comparativa si coincide el numero de la placa
                if ($numplaca == 1 || $numplaca == 2) {
                    //Retorno del resultado
                    return true;
                }
                break;
            case 'Martes':
                //Comparativa si coincide el numero de la placa
                if ($numplaca == 3 || $numplaca == 4) {
                    //Retorno del resultado
                    return true;
                }
                break;
            case 'Miercoles':
                //Comparativa si coincide el numero de la placa
                if ($numplaca == 5 || $numplaca == 6) {
                    //Retorno del resultado
                    return true;
                }
                break;
            case 'Jueves':
                //Comparativa si coincide el numero de la placa
                if ($numplaca == 7 || $numplaca == 8) {
                    //Retorno del resultado
                    return true;
                }
                break;
            case 'Viernes':
                //Comparativa si coincide el numero de la placa
                if ($numplaca == 9 || $numplaca == 0) {
                    //Retorno del resultado
                    return true;
                }
                break;
            case 'Sabado':
                //Retorno del resultado
                return false;
                break;
            case 'Domingo':
                //Retorno del resultado
                return false;
                break;
        }
    } 
}
