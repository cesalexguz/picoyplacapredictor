{{-- El layout de la pagina se encuentra en la vista index en la carpeta views --}}

@extends('index')

@section('content')

  {{-- Vista con la pantalla en caso de error o en caso de enviar el formulario vacio --}}

  <div class="container section">
    <div class="row">
      <form class="col s12" method="GET" action="/">
        <h2 class="header">¡Lo sentimos, hemos tenido un problema!</h2><br>
        <div class="row">
          <div class="col s12">
            <h5>Por favor verifique que todos los campos han sido llenados e intente de nuevo. </h5>
            <h5>En caso de persistir con el problema. Por favor contacte con el servicio técnico. </h5><br>
            <button class="btn waves-effect waves-light blue darken-2" type="submit">Nueva Consulta
            </button>
          </div>
        </div>
      </form>
    </div>
  </div>

@endsection

    

