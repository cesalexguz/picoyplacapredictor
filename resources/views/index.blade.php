<!DOCTYPE html>
<html lang="es">

  <head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Pico y Placa</title>

    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/materialize.min.css')}}">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">

  </head>

  <body>

    <!-- Navbar -->
    <nav>
      <div class="nav-wrapper blue darken-3">
        <a href="#" class="brand-logo center">Pico y Placa Predictor</a>
      </div>
    </nav>

    <!-- Body Content -->
    @yield('content')

    <!-- Footer -->
    <footer class="footer-copyright blue darken-3">
        <div class="container">
          <div class="row">
            <div class="col l6 s12">
              <h5 class="white-text">Made by César Guzmán</h5>
              <p class="grey-text text-lighten-4">Laravel</p>
            </div>
          </div>
        </div>
    </footer>

    <!-- Compiled and minified JavaScript -->
    <script src="{{asset('js/materialize.js')}}"></script>

    <!-- Materlalize Functions Load -->
    <script>

      document.addEventListener('DOMContentLoaded', function() {
        M.AutoInit();
      });

    </script>

  </body>

</html>
