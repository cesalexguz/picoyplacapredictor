{{-- El layout de la pagina se encuentra en la vista index en la carpeta views --}}

@extends('index')

@section('content')

  {{-- Vista con el formulario de consulta --}}

  <div class="container section">
    <div class="row">
      <form class="col s12" method="POST" action="/calcularPicoPlaca">
        @csrf
        <h2 class="header">Consulte su Pico y Placa</h2><br>
        <div class="row">
          <div class="col s12">
            <h6>Por favor Ingrese la información solicitada</h2><br>
            <div class="input-field col s12">
              <input placeholder="Ingrese la placa de su vehículo" id="placa" name="placa" type="text" class="validate" required>
              <label for="placa">Ingrese la placa de su vehículo</label><br>
            </div>
            <div class="input-field col s6">
              <input type="text" class="datepicker" id="fecha" name="fecha" class="validate" required>
              <label for="fecha">Ingrese el día</label>
            </div>
            <div class="input-field col s6">
              <input type="text" class="timepicker" id="hora" name="hora" class="validate" required>
              <label for="hora">Ingrese el hora</label>
            </div>
            <button class="btn waves-effect waves-light blue darken-2" type="submit" name="action">Enviar
              <i class="material-icons right">send</i>
            </button>
          </div>
        </div>
      </form>
    </div>
  </div>

@endsection

    

