{{-- El layout de la pagina se encuentra en la vista index en la carpeta views --}}

@extends('index')

@section('content')

  {{-- Vista con la pantalla de resultado --}}

  <div class="container section">
    <div class="row">
      <form class="col s12" method="GET" action="/">
        <h2 class="header">Su coche <b>{{$resultado}}</b> en el día y hora seleccionada.</h2><br>
        <div class="row">
          <div class="col s12">
            <h6>¡Gracias por usar nuestra plataforma! </h6><br>
            <button class="btn waves-effect waves-light blue darken-2" type="submit">Nueva Consulta
            </button>
          </div>
        </div>
      </form>
    </div>
  </div>

@endsection

    

