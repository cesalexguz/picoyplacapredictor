<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Ruta para llamar a la pantalla principal
Route::get('/', function () {
    return view('form');
});

// Ruta para llamar al metodo del calculo del pico y placa, en el controlador PredictorController
Route::post('/calcularPicoPlaca', 'PredictorController@calcularPicoyPlaca');
